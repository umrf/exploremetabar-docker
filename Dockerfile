FROM rocker/r-ver:4.1.1
RUN apt-get update && apt-get install -y  git-core libcurl4-openssl-dev libgit2-dev libglpk-dev libgmp-dev libicu-dev libpng-dev libssl-dev libxml2-dev make pandoc pandoc-citeproc zlib1g-dev && rm -rf /var/lib/apt/lists/*
RUN echo "options(repos = c(CRAN = 'https://cran.rstudio.com/'), download.file.method = 'libcurl', Ncpus = 4)" >> /usr/local/lib/R/etc/Rprofile.site
RUN R -e 'install.packages("remotes")'
RUN Rscript -e 'remotes::install_version("tibble",upgrade="never", version = "3.1.6")'
RUN Rscript -e 'remotes::install_version("glue",upgrade="never", version = "1.6.2")'
RUN Rscript -e 'remotes::install_version("processx",upgrade="never", version = "3.5.2")'
RUN Rscript -e 'remotes::install_version("htmltools",upgrade="never", version = "0.5.2")'
RUN Rscript -e 'remotes::install_version("ggplot2",upgrade="never", version = "3.3.5")'
RUN Rscript -e 'remotes::install_version("purrr",upgrade="never", version = "0.3.4")'
RUN Rscript -e 'remotes::install_version("htmlwidgets",upgrade="never", version = "1.5.4")'
RUN Rscript -e 'remotes::install_version("knitr",upgrade="never", version = "1.33")'
RUN Rscript -e 'remotes::install_version("dplyr",upgrade="never", version = "1.0.7")'
RUN Rscript -e 'remotes::install_version("bslib",upgrade="never", version = "0.3.1")'
RUN Rscript -e 'remotes::install_version("pkgload",upgrade="never", version = "1.2.1")'
RUN Rscript -e 'remotes::install_version("forcats",upgrade="never", version = "0.5.1")'
RUN Rscript -e 'remotes::install_version("tidyr",upgrade="never", version = "1.1.3")'
RUN Rscript -e 'remotes::install_version("shiny",upgrade="never", version = "1.7.1")'
RUN Rscript -e 'remotes::install_version("futile.logger",upgrade="never", version = "1.4.3")'
RUN Rscript -e 'remotes::install_version("vegan",upgrade="never", version = "2.5-7")'
RUN Rscript -e 'remotes::install_version("config",upgrade="never", version = "0.3.1")'
RUN Rscript -e 'remotes::install_version("testthat",upgrade="never", version = "3.0.4")'
RUN Rscript -e 'remotes::install_version("rmarkdown",upgrade="never", version = "2.10")'
RUN Rscript -e 'remotes::install_version("VennDiagram",upgrade="never", version = "1.6.20")'
RUN Rscript -e 'remotes::install_version("venn",upgrade="never", version = "1.10")'
RUN Rscript -e 'remotes::install_version("shinyjs",upgrade="never", version = "2.0.0")'
RUN Rscript -e 'remotes::install_version("shinydashboard",upgrade="never", version = "0.7.1")'
RUN Rscript -e 'remotes::install_version("shinycustomloader",upgrade="never", version = "0.9.0")'
RUN Rscript -e 'remotes::install_version("shinyBS",upgrade="never", version = "0.61")'
RUN Rscript -e 'remotes::install_version("shinyalert",upgrade="never", version = "3.0.0")'
RUN Rscript -e 'remotes::install_version("reshape2",upgrade="never", version = "1.4.4")'
RUN Rscript -e 'remotes::install_version("plotly",upgrade="never", version = "4.9.4.1")'
RUN Rscript -e 'remotes::install_version("gtools",upgrade="never", version = "3.9.2")'
RUN Rscript -e 'remotes::install_version("golem",upgrade="never", version = "0.3.1")'
RUN Rscript -e 'remotes::install_version("DT",upgrade="never", version = "0.21")'
RUN Rscript -e 'remotes::install_version("broom",upgrade="never", version = "0.7.9")'
RUN Rscript -e 'remotes::install_version("agricolae",upgrade="never", version = "1.3-5")'
RUN Rscript -e 'remotes::install_github("dreamRs/shinyWidgets@5b3810d3e42ab885baecd5fe80480e8381758766")'
RUN Rscript -e 'install.packages("BiocManager")'
RUN Rscript -e 'BiocManager::install("rhdf5")'
RUN Rscript -e 'BiocManager::install("phyloseq")'
RUN Rscript -e 'install.packages("devtools")'
RUN Rscript -e 'devtools::install_github("pmartinezarbizu/pairwiseAdonis/pairwiseAdonis")'
RUN Rscript -e 'BiocManager::install("microbiome")'
RUN Rscript -e 'BiocManager::install("metagenomeSeq")'
RUN Rscript -e 'remotes::install_github("grunwaldlab/metacoder@8146849be7b243b9fc9f50fa735e8a00f8803e6f")'
RUN Rscript -e 'remotes::install_github("mikelove/DESeq2@e414a2fba3db15b77db2f590ad189ae24ff9af42")'
RUN Rscript -e 'BiocManager::install("Biostrings")'
RUN Rscript -e 'BiocManager::install("Biobase")'
RUN mkdir /build_zone
ADD . /build_zone
WORKDIR /build_zone

RUN R -e 'Sys.setenv(GITHUB_PAT="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");remotes::install_local(upgrade="never")'
RUN rm -rf /build_zone
EXPOSE 3838

# CMD R

CMD R -e "options('shiny.port'=3838,shiny.host='0.0.0.0');library(vegan);library(ExploreMetabar);library(shinyalert);library(shiny);sessionInfo();ExploreMetabar::run_app()"
